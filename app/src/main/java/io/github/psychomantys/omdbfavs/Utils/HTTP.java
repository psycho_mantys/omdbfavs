package io.github.psychomantys.omdbfavs.Utils;

import android.net.Uri;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

/**
 * Criado para auxiliar as consultas HTTP. Como é pouca coisa, uma parte das informações do OMDB vão
 * ficar aqui também
 */
public class HTTP {
    // TODO: Criar um menu de configuração para inserir essa
    /*
     * Chave da API do OMDB, precisa porque todas as requisições são pagas agora
     */
    private final static String APIKEY = "dab75622";
    private final static String PARAM_APIKEY="apikey";

    /*
     * Pametro para pesquisar por titulo na API do OMDB
     */
    private final static String PARAM_SEARCH = "t";

    private final static String OMDB_BASE_API_URL = "http://www.omdbapi.com";

    /**
     *  Constroi uma URL formada com a requisição que se deve fazer para o OMDB
     *
     *  @param movieTitle Pedaço do titulo do filme a ser pesquisado no OMDB.
     *  @return A URL com o resultado da pesqueisa que deve ser acessada
     */
    public static URL buildOMDBUrl( String movieTitle ){
        Uri uri = Uri.parse(OMDB_BASE_API_URL).buildUpon()
                .appendQueryParameter(PARAM_APIKEY,APIKEY)
                .appendQueryParameter(PARAM_SEARCH, movieTitle)
                .build();

//        Log.i("URL: ",uri.toString());

        URL url=null;

        try{
            url = new URL(uri.toString());
        }catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return url;
    }


    /**
     * Metodo que dado uma URL, ele consegue pegar todo o conteudo da resposta do HTTP daquele
     * endereço
     *
     * @param url Endereço que vai ser requisitado o conteudo
     * @return O conteudo da requisição HTTP
     * @throws IOException Se tiver algum problema na rede
     */
    public static String getHTTPResponse( URL url ) throws IOException {
        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
        try {
            InputStream in = urlConnection.getInputStream();

            Scanner scanner = new Scanner(in);
            scanner.useDelimiter("\\A");

            boolean hasInput = scanner.hasNext();
            if (hasInput) {
                return scanner.next();
            } else {
                return null;
            }
        } finally {
            urlConnection.disconnect();
        }
    }

}
