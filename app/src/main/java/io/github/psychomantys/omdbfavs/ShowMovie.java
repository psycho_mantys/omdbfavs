package io.github.psychomantys.omdbfavs;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import io.github.psychomantys.omdbfavs.Data.FavMoviesDBHelper;

import static io.github.psychomantys.omdbfavs.Utils.Bitmap.bytesToBitmap;

public class ShowMovie extends AppCompatActivity {

    // Referencias de campos do layout
    private LinearLayout mDisplay;
    private TextView mErrorMessage;
    //private ProgressBar mLoading;

    // Referencias aos dados do filme
    private TextView mMovieName;
    private TextView mMovieGenre;
    private TextView mMoviePlot;
    private TextView mMovieActors;
    private ImageView mMoviePoster;

    // Placeholder do poster
    private Drawable placeholder;

    // Dados em JSON do filme
    private JSONObject mMovieData;
    // Nome do filme
    private String mTitle;

    // Banco de dado com filmes Favoritos
    private SQLiteDatabase mDB;
    private FavMoviesDBHelper mDBHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_movie);

        // Encontra referencias aos campos do layout
        mDisplay = (LinearLayout) findViewById(R.id.display);
        mErrorMessage = (TextView) findViewById(R.id.tv_error_message);
        //mLoading = (ProgressBar) findViewById(R.id.pb_loading);

        // Encontrando os campos a preencher com dados do filme no layout
        mMovieName = (TextView) findViewById(R.id.tv_movie_name);
        mMovieGenre = (TextView) findViewById(R.id.tv_movie_genre);
        mMoviePlot = (TextView) findViewById(R.id.tv_movie_plot);
        mMovieActors = (TextView) findViewById(R.id.tv_movie_actors);
        mMoviePoster = (ImageView) findViewById(R.id.iv_poster);

        // Cria banco de dados se ele estiver sendo executado pela primeira vez
        mDBHelper = new FavMoviesDBHelper(this);

        mDB = mDBHelper.getWritableDatabase();

        // Obter os dados do filme a ser exibido
        Intent parentIntent = getIntent();
        if ( parentIntent.hasExtra("movie_data") ){
            try{
                mMovieData = new JSONObject( parentIntent.getStringExtra("movie_data") );
            }catch (JSONException e){
                e.printStackTrace();
                showError();
                return;
            }
        } else {
            // Não foi obtido nenhum parametro, então existe um erro
            showError();
            return;
        }
        // Vai ser importante no futuro, quando não estiver com internet
        if ( parentIntent.hasExtra("movie_poster")
                && ( parentIntent.getByteArrayExtra("movie_poster")!=null ) ) {
            // Se tiver passado uma imagem, usa ela como placeholder, para o caso de não ter
            // internet
            Bitmap img=bytesToBitmap(parentIntent.getByteArrayExtra("movie_poster"));
            placeholder=new BitmapDrawable(getResources(),img);
        }

        loadMovieData();
    }


    void loadMovieData() {
        try{
            mTitle=(mMovieData.getString("Title"));
            mMovieName.setText(mMovieData.getString("Title")+"("+mMovieData.getString("Year")+")");
            mMovieGenre.setText(mMovieData.getString("Genre"));
            mMoviePlot.setText(mMovieData.getString("Plot"));
            mMovieActors.setText(mMovieData.getString("Actors"));
            String poster=mMovieData.getString("Poster");

            if( !poster.equals("N/A") ) {
                Picasso.with(ShowMovie.this).load(mMovieData.getString("Poster")).
                        placeholder(placeholder).into(mMoviePoster);
            }else{
                mMoviePoster.setImageResource(R.mipmap.ic_launcher);
            }
        }catch (JSONException e){
            e.printStackTrace();
            showError();
            return;
        }
        showMovie();
    }

    /*
     * Metodo para facilitar a exibição de erros. Quando houver um erro, ele vai desativar os campos
     * desnecessarios e mostrar apenas o erro na tela.
     */
    private void showError() {
        mDisplay.setVisibility(View.INVISIBLE);
        mErrorMessage.setVisibility(View.VISIBLE);
    }

    /*
     * Maetodo para mostrar na tela os filmes. Quando esse metodo é chamado, ele vai habilitar o
     * campo para mostrar a lista de filmes e desabilitar os erros.
     */
    private void showMovie(){
        mErrorMessage.setVisibility(View.INVISIBLE);
        mDisplay.setVisibility(View.VISIBLE);
    }

    /**
     * Dado os dados de um filme, passa para a proxima tela para mostra esse filme
     *
     * @param parentContext Contexto que esta chamando a exibição do filme
     * @param movie_json O json do filme em uma string
     * @param movie_poster Poster em Bitmap do filme em um array de bytes. Passe null se for usado.
     */
    public static void showMovie( Context parentContext, String movie_json, byte[] movie_poster){
        Intent startShowMovie = new Intent(parentContext, ShowMovie.class);
        startShowMovie.putExtra("movie_data", movie_json);
        startShowMovie.putExtra("movie_poster", movie_poster);
        parentContext.startActivity(startShowMovie);
    }


    @Override
    protected void onResume() {
        super.onResume();
        // Quando voltar, reabre o bando e da refresh na interface
        mDB = mDBHelper.getReadableDatabase();
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Qunado sair, fecha o banco
        mDB.close();
    }

    // Adicionando o menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.movie, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemSelected = item.getItemId();
        // Ir para a lista de favoritos selecionado
        if ( itemSelected == R.id.goto_favs ){
            Intent startFavs = new Intent(ShowMovie.this, FavOMDB.class);
            startActivity(startFavs);
            return true;
        }
        // Adicionar a lista de favoritos selecionado
        if ( itemSelected == R.id.add_fav ){
            BitmapDrawable d = (BitmapDrawable) mMoviePoster.getDrawable();
            Bitmap b=null;
            if( d!=null ){
                b=d.getBitmap();
            }

            if ( FavMoviesDBHelper.addFavMovie(mDB, mMovieData.toString(), b) > -1 ){
                Toast.makeText(this, R.string.movie_add_success, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, R.string.error_message, Toast.LENGTH_LONG).show();
            }
        }
        // Adicionar a lista de favoritos selecionado
        if ( itemSelected == R.id.remove_fav ){
            if ( FavMoviesDBHelper.removebyTitleFavMovie(mDB, mTitle ) ){
                Toast.makeText(this, R.string.movie_rm_success, Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(this, R.string.error_message, Toast.LENGTH_LONG).show();
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
