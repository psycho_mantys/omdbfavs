package io.github.psychomantys.omdbfavs.Data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.Bitmap;

import org.json.JSONException;
import org.json.JSONObject;

import io.github.psychomantys.omdbfavs.Data.FavMoviesContract.*;

import static io.github.psychomantys.omdbfavs.Utils.Bitmap.bitmapToBytes;

/**
 * Classe para facilitar a manipulação do banco de dados
 */
public class FavMoviesDBHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "favmovies.db";

    // Versão usada do schema atual. Se ele for alterado, deve ser incrementada essa variavel
    private static final int DB_VERSION = 1;

    // Construtor
    public FavMoviesDBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String SQL_CREATE_TABLE = "CREATE TABLE " + FavMoviesEntry.TABLE_NAME + " ( " +
                FavMoviesEntry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                FavMoviesEntry.COLUMN_JSON_DATA + " TEXT NOT NULL, " +
                FavMoviesEntry.COLUMN_TITLE + " TEXT UNIQUE NOT NULL, " +
                FavMoviesEntry.COLUMN_POSTER_DATA + " BLOB " +
                "); ";
        db.execSQL(SQL_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        /*
         * Não preciso fazer upgrade porque é a primeira versão do DB
         */
    }

    /**
     * Adiciona um filme da sua lista de favoritos
     *
     * @param db Banco de dados que vai ser realizado a operação
     * @param json   Bando de dados
     * @param poster Bitmap do poster do filme para futuro cache
     * @return O ID da coluna que o filme foi inserido ou -1 se tiver um erro
     */
    public static long addFavMovie(SQLiteDatabase db, String json, Bitmap poster) {
        JSONObject movieData;
        String title;
        String poster_url;
        try{
            movieData = new JSONObject( json );
            title=movieData.getString("Title");
            poster_url=movieData.getString("Poster");
        }catch (JSONException e){
            e.printStackTrace();
            return -1;
        }

        ContentValues cv = new ContentValues();
        cv.put(FavMoviesEntry.COLUMN_JSON_DATA, json);
        cv.put(FavMoviesEntry.COLUMN_TITLE, title);

        // Caso não exista porter no filme
        if ( poster_url.equals("N/A") ){
            cv.put(FavMoviesEntry.COLUMN_POSTER_DATA, (Byte)null );
        }else {
            cv.put(FavMoviesEntry.COLUMN_POSTER_DATA, bitmapToBytes(poster));
        }
        return db.insert(FavMoviesEntry.TABLE_NAME, null, cv);
    }

    /**
     * Remove um filme da sua lista de favoritos pelo Id dele
     *
     * @param db Bando de dados
     * @param id Chave primaria do filme a ser removido
     * @return Verdadeiro se o filme foi removido
     */
    public static boolean removebyIdFavMovie(SQLiteDatabase db, long id) {
        return db.delete(FavMoviesEntry.TABLE_NAME, FavMoviesEntry._ID + "=" + id, null) > 0;
    }
    /**
     * Remove um filme da sua lista de favoritos pelo Id titulo
     *
     * @param db Bando de dados
     * @param title Titulo do filme a ser removido
     * @return Verdadeiro se o filme foi removido
     */
    public static boolean removebyTitleFavMovie(SQLiteDatabase db, String title) {
        return db.delete(FavMoviesEntry.TABLE_NAME, FavMoviesEntry.COLUMN_TITLE + "=?",new String[]{title}) > 0;
    }

    /**
     *
     */
    public static Cursor getAllFavMovies( SQLiteDatabase db ){
        // Consulta todos os favoritos ordenando pelo titulo
        return db.query(
                FavMoviesEntry.TABLE_NAME,
                null,
                null,
                null,
                null,
                null,
                FavMoviesEntry.COLUMN_TITLE + " COLLATE NOCASE ASC"
        );
    }
}