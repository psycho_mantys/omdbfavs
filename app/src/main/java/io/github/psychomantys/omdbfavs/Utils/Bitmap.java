package io.github.psychomantys.omdbfavs.Utils;

import android.graphics.BitmapFactory;

import java.io.ByteArrayOutputStream;

/**
 * Classe para ajudar a manipular e converter os bitmap
 */
public class Bitmap {
    /**
     * Função para converter um bitmap para um array de bytes, para facilitar salvar no DB
     *
     * @param bitmap Imagem a ser convertida
     * @return Um array de bytes com a imagem
     */
    public static byte[] bitmapToBytes(android.graphics.Bitmap bitmap) {
        if( bitmap==null ){
            return null;
        }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        bitmap.compress(android.graphics.Bitmap.CompressFormat.PNG, 0, outputStream);
        return outputStream.toByteArray();
    }


    /**
     * Converte de byte[] para um bitmap. Ideal para quando retira do banco de dados a imagem.
     *
     * @param img Array de bytes
     * @return Bitmap da imagem convertida
     */
    public static android.graphics.Bitmap bytesToBitmap(byte[] img) {
        if ( img==null ){
            return null;
        }
        return BitmapFactory.decodeByteArray(img, 0, img.length);
    }
}
