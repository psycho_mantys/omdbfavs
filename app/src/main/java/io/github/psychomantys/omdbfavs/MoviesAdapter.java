package io.github.psychomantys.omdbfavs;


import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import io.github.psychomantys.omdbfavs.Data.FavMoviesContract;

import static io.github.psychomantys.omdbfavs.Utils.Bitmap.bytesToBitmap;

/**
 * Classe para mostrar uma lista de filmes
 */
public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MoviesAdapterViewHolder> {
    // Iterador no banco de dados que contem a lista de filmes favoritos
    private Cursor mCursor;
    private Context mContext;
    // Quem vai tratar os eventos de pressionar mouse
    private MoviesAdapterOnClickHandler mClickHandler;

    /**
     * Construtor do Adapter
     *
     * @param clickHandler Quem vai tratar quando um objeto da lista tiver um evento de click
     * @param context Contexto que vai ser renderizado os itens
     * @param cursor Iterador no banco de dados que contem a lista de filmes favoritos
     */
    public MoviesAdapter(MoviesAdapterOnClickHandler clickHandler, Context context, Cursor cursor) {
        mContext=context;
        mCursor=cursor;
        mClickHandler=clickHandler;
    }

    /**
     * Interface da classe que vai tratar quando o mouse for pressionado em cima de um item da lista
     * de filmes favoritos.
     */
    public interface MoviesAdapterOnClickHandler {
        /**
         * Quando for disparado o click no item da lista, esse metodo vai ser chamado passando o
         * cursor
         *
         * @param cursor Cursor com os dados do item da lista que foi dado o click
         */
        void onClick(Cursor cursor);
    }

    @Override
    public MoviesAdapterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.fav_omdb_list_item, parent, false);
        return new MoviesAdapterViewHolder(view);
    }
    @Override
    public void onBindViewHolder(MoviesAdapterViewHolder holder, int position) {
        String date=null;
        String poster_url=null;
        Drawable placeholder=null;
        long id;

        if (!mCursor.moveToPosition(position))
            return;

        // Obtem as informações do filme
        String title = mCursor.getString(mCursor.getColumnIndex(FavMoviesContract.FavMoviesEntry.COLUMN_TITLE ));

        // Pega a chave primaria do filme
        id = mCursor.getLong(mCursor.getColumnIndex(FavMoviesContract.FavMoviesEntry._ID));

        // Informações que estão no JSON
        try{
            JSONObject j=new JSONObject(mCursor.getString(mCursor.getColumnIndex(FavMoviesContract.FavMoviesEntry.COLUMN_JSON_DATA )));

            date=j.getString("Year");
            poster_url=j.getString("Poster");

            byte [] poster_blob=mCursor.getBlob(
                    mCursor.getColumnIndex(FavMoviesContract.FavMoviesEntry.COLUMN_POSTER_DATA));
            Bitmap img=bytesToBitmap(poster_blob);
            // Se não tiver um poster valido ou algo assim, coloca um placeholder no lugar
            if( img!=null ) {
                placeholder=new BitmapDrawable(mContext.getResources(), img);
            } else {
                placeholder=ContextCompat.getDrawable(mContext, R.mipmap.ic_launcher);
            }
        }catch (JSONException e){
            e.printStackTrace();
        }

        holder.mMovieName.setText(title+" ("+date+")");
        Picasso.with(mContext).load(poster_url).
                placeholder(placeholder).into(holder.mMoviePoster);
        holder.itemView.setTag(id);
    }


    @Override
    public int getItemCount() {
        return mCursor.getCount();
    }


    /**
     * Troca o cursor atual do adapter por um novo e diz para a interface para renderizar de novo
     * Ideal para usar depois de remover ou adicionar com a lista sendo visualizada.
     *
     * @param newCursor Favoritos atualizados
     */
    public void swapCursor(Cursor newCursor) {
        if (mCursor != null) {
            mCursor.close();
        }
        mCursor = newCursor;
        if ( newCursor!=null ) {
            notifyDataSetChanged();
        }
    }


    public class MoviesAdapterViewHolder extends RecyclerView.ViewHolder implements OnClickListener {
        private final TextView mMovieName;
        private final ImageView mMoviePoster;


        /**
         * Contrutor do ViewHolder, para pegar as referencias externas dos views
         *
         * @param view O view que esta sendo criado
         */
        public MoviesAdapterViewHolder(View view) {
            super(view);
            mMovieName = (TextView) view.findViewById(R.id.tv_movie_name);
            mMoviePoster = (ImageView) view.findViewById(R.id.iv_movie_poster);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            // Move cursor para o item que esta disparando o evento de click
            mCursor.moveToPosition(getAdapterPosition());
            // "Rotear" os clicks para quem vai tratar, passando o cursor na posição certa
            mClickHandler.onClick(mCursor);
        }
    }
}
