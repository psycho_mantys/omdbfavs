package io.github.psychomantys.omdbfavs;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import io.github.psychomantys.omdbfavs.Data.FavMoviesContract;
import io.github.psychomantys.omdbfavs.Data.FavMoviesDBHelper;
import io.github.psychomantys.omdbfavs.MoviesAdapter.MoviesAdapterOnClickHandler;

import static io.github.psychomantys.omdbfavs.ShowMovie.showMovie;


public class FavOMDB extends AppCompatActivity implements MoviesAdapterOnClickHandler {


    private ProgressBar mLoading;
    private TextView mErrorMessage;

    private RecyclerView mReclycerView;

    private SQLiteDatabase mDB;
    private FavMoviesDBHelper mDBHelper;

    private MoviesAdapter mMoviesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fav_omdb);

        // Banco de dados
        mDBHelper = new FavMoviesDBHelper(this);
        mDB = mDBHelper.getReadableDatabase();

        mReclycerView = (RecyclerView) findViewById(R.id.rv_favs_movies);

        mReclycerView.setLayoutManager(new LinearLayoutManager(this));

        Cursor cursor = FavMoviesDBHelper.getAllFavMovies(mDB);

        mMoviesAdapter = new MoviesAdapter(this, this, cursor);
        mReclycerView.setAdapter(mMoviesAdapter);

        // Adicionar função de deslizar item da lista na tela de favoritos e apagar ele
        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT|ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(RecyclerView rv, RecyclerView.ViewHolder vh, RecyclerView.ViewHolder t) {
                // Não vou utilizar isso
                return false;
            }

            // O metodo para swipe. Basicamente pegar a tag que eu coloquei no item, que é
            // convenientemente o ID da chave primaria do banco de dados, e apagar esse item
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                long id=(long) viewHolder.itemView.getTag();
                FavMoviesDBHelper.removebyIdFavMovie(mDB,id);
                mMoviesAdapter.swapCursor(FavMoviesDBHelper.getAllFavMovies(mDB));
            }
        }).attachToRecyclerView(mReclycerView);
    }


    @Override
    protected void onResume() {
        super.onResume();
        // Quando voltar, reabre o bando e da refresh na interface
        mDB = mDBHelper.getReadableDatabase();
        mMoviesAdapter.swapCursor(FavMoviesDBHelper.getAllFavMovies(mDB));
    }

    @Override
    protected void onStop() {
        super.onStop();
        // Qunado sair, fecha o banco
        mDB.close();
    }

    @Override
    public void onClick(Cursor cursor) {
        String movie=
                cursor.getString(cursor.getColumnIndex(FavMoviesContract.FavMoviesEntry.COLUMN_JSON_DATA ));
        showMovie(this,
                cursor.getString(cursor.getColumnIndex(FavMoviesContract.FavMoviesEntry.COLUMN_JSON_DATA )),
                cursor.getBlob(cursor.getColumnIndex(FavMoviesContract.FavMoviesEntry.COLUMN_POSTER_DATA)));
    }

    private void showMovies(){
        mErrorMessage.setVisibility(View.INVISIBLE);
        mReclycerView.setVisibility(View.VISIBLE);
    }
    private void showError(){
        mReclycerView.setVisibility(View.INVISIBLE);
        mErrorMessage.setVisibility(View.VISIBLE);
    }
}
