package io.github.psychomantys.omdbfavs.Data;


import android.provider.BaseColumns;

/**
 * Contrato do banco de dados dos filmes favoritos salvos pelo usuario
 */
public class FavMoviesContract {
    public static final class FavMoviesEntry implements BaseColumns {
        public static final String TABLE_NAME = "favmovielist";
        /**
         *  Os dados extraidos do OMDB no formato JSON deles. Como são muitos campos, vou colocar
         *  o JSON inteiro aqui e não criar os campos de 1 por 1.
          */
        public static final String COLUMN_JSON_DATA = "jsonData";
        /**
         * Titulo do filme adicionado. Pode ser um pouco redundante, mas facilita na manipulação do
         * banco de dados
         */
        public static final String COLUMN_TITLE = "title";
        /**
         * O poster do filme em formato do bitmap. Idealmente não é bom colocar imagens no banco de
         * dados, mas essas imagens são pequenas o que faz que não se tenha OOMs e preciso delas
         * para quando o aplicativo estiver sem internet.
         */
        public static final String COLUMN_POSTER_DATA = "poster";
    }
}
