package io.github.psychomantys.omdbfavs;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.net.URL;

import io.github.psychomantys.omdbfavs.Utils.HTTP;

public class MainActivity extends AppCompatActivity {

    private Button mSearchButton;
    private TextView mSearchEntry;
    private ProgressBar mLoading;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSearchButton = (Button) findViewById(R.id.b_search);
        mSearchEntry = (TextView) findViewById(R.id.et_search_entry);
        mLoading = (ProgressBar) findViewById(R.id.pb_loading);

        /*
         * Conectando o botão da tela inicial com a procura de filme no OMDB
         */
        mSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            searchMovie();
            }
        });
//
//        mSearchEntry.setOnEditorActionListener(new TextView.OnEditorActionListener(){
//            @Override
//            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                Log.v("click  ", ""+actionId);
//                if( actionId==EditorInfo.IME_ACTION_GO ){
//                    searchMovie();
//                    return true;
//                }
//                return false;
//            }
//        });
    }


    /**
     * Metodo que pesquisa um filme se os campos da interface estiverem preenchidos certo
     */
    public void searchMovie(){
        // Monta a URL e faz a requisição em uma outra thread
        URL query = HTTP.buildOMDBUrl(mSearchEntry.getText().toString());
        new OMDBHTTPTask().execute(query);
    }

    /**
     * Classe para executar a requisição HTTP para a API do OMDB de forma assincrona sem ser na
     * thread principal
     */
    private class OMDBHTTPTask extends AsyncTask<URL, Void, String> {
        /**
         * Antes de começar a tarefa, mostra uma barra de progresso
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mLoading.setVisibility(View.VISIBLE);
        }
        @Override
        protected String doInBackground(URL... params) {
            URL url=params[0];
            String HTTPContent=null;
            try{
                HTTPContent=HTTP.getHTTPResponse(url);
            }catch (IOException e){
                e.printStackTrace();
            }
            return HTTPContent;
        }

        /*
          * Metodo para facilitar a exibição de erros. Quando houver um erro, ele vai desativar os campos
          * desnecessarios e mostrar apenas o erro na tela.
          */
        private void showError() {
            Toast.makeText(MainActivity.this, R.string.error_message, Toast.LENGTH_LONG).show();
        }

        /**
         * Metodo executado depois que a tarefa é executada. Nesse caso troca de tela para mostrar
         * o filme.
         *
         * @param HTTPContent Conteudo da pesquisa a API.
         */
        @Override
        protected void onPostExecute(String HTTPContent) {
            // Como a tarefa terminou, para o progressbar
            mLoading.setVisibility(View.INVISIBLE);
            // Se não tiver nada, ou  apesquisa deu errado ou não tem esse filme
            if ( HTTPContent!=null && !HTTPContent.equals("") && !HTTPContent.equals("{\"Response\":\"False\",\"Error\":\"Movie not found!\"}")){
                ShowMovie.showMovie( MainActivity.this, HTTPContent, null );
            }else{
                showError();
            }
        }
    }

    // Adicionando o menu
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemSelected = item.getItemId();
        if ( itemSelected == R.id.goto_favs ){
            Intent startFavs = new Intent(MainActivity.this, FavOMDB.class);
            startActivity(startFavs);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
